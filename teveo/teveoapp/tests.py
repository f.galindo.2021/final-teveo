from django.test import TestCase, Client
from .models import Camera, Comment, User
from django.utils import timezone
import random
import string


# Create your tests here.


class GetTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        user_id_1 = ''.join(random.choices(string.ascii_lowercase + string.digits, k=32))
        user_id_2 = ''.join(random.choices(string.ascii_lowercase + string.digits, k=32))
        cls.camera_1 = Camera.objects.create(camera_id='Cam1', lat='lat1', lon='lon1',
                                             camera_view='https://ctraficomovilidad.malaga'
                                                         '.eu/recursos/movilidad'
                                                         '/camaras_trafico/TV-24.jpg',
                                             camera_location='Madrid', comments_number=1, id=1)
        cls.camera_2 = Camera.objects.create(camera_id='Cam2', lat='lat1', lon='lon1',
                                             camera_view='https://informo.madrid.es/cameras'
                                                         '/Camara06305.jpg',
                                             camera_location='Madrid', comments_number=2, id=2)
        cls.user_1 = User.objects.create(username='Usuario 1', user_id=user_id_1, id=1)
        cls.user_2 = User.objects.create(username='Usuario 2', user_id=user_id_2, id=2)
        cls.comment_1 = Comment.objects.create(comment='Comentario 1', date=timezone.now(), user_id=1,
                                               camera_id_id=1)
        cls.comment_2 = Comment.objects.create(comment='Comentario 2', date=timezone.now(), user_id=2,
                                               camera_id_id=2)
        cls.c = Client()

    def test_index(self):
        response = self.c.get('/teveo/')
        self.assertEquals(response.status_code, 200)
        self.assertIn('comments', response.context)
        self.assertIn('num_cameras', response.context)
        self.assertIn('num_comments', response.context)
        self.assertTemplateUsed(response, 'teveoapp/index.html')

    def test_descargar_camaras(self):
        response = self.c.post('/teveo/camaras/', {'Lista': '1'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teveoapp/camaras.html')

    def test_get_camara_1(self):
        response = self.c.get('/teveo/Cam1/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teveoapp/camara.html')

    def test_get_camara_2(self):
        response = self.c.get('/teveo/Cam2/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teveoapp/camara.html')

    def test_get_camara_1_dyn(self):
        response = self.c.get('/teveo/Cam1-dyn/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teveoapp/camaradyn.html')

    def test_get_camara_2_dyn(self):
        response = self.c.get('/teveo/Cam2-dyn/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teveoapp/camaradyn.html')

    def test_get_comentario(self):
        response = self.c.get('/teveo/comentario/?cam_id=Cam1')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teveoapp/comentario.html')

    def test_post_comentario(self):
        response = self.c.post('/teveo/comentario/?cam_id=Cam1', {'comment': 'Comentario Cam1'})
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'teveoapp/comentario.html')

    def test_get_comentario_that_not_exist(self):
        response = self.c.get('/teveo/comentario/?cam_id=Cam1snaskjnsajn')
        self.assertEqual(response.status_code, 404)
        self.assertTemplateUsed(response, 'teveoapp/notfound.html')

    def test_get_ayuda(self):
        response = self.c.get('/teveo/ayuda/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teveoapp/ayuda.html')

    def test_get_json(self):
        response = self.c.get('/teveo/Cam1.json')
        self.assertEqual(response.status_code, 200)
