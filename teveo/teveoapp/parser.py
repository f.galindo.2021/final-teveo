from xml.sax import ContentHandler
from .models import Camera


class Parserl1(ContentHandler):

    def __init__(self):
        self.video_id = False
        self.url = False
        self.lugar = False
        self.coord = False
        self.in_content = False
        self.in_cameras = False
        self.cam_id = 'LIS1-'
        self.cam_view = ''
        self.cam_lat = ''
        self.cam_lon = ''
        self.cam_location = ''

    def startElement(self, name, attrs):
        if name == 'camaras':
            self.in_cameras = True
        elif name == 'camara':
            self.in_content = True
        elif name == 'id' and self.in_cameras and self.in_content:
            self.video_id = True
        elif name == 'src' and self.in_cameras and self.in_content:
            self.url = True
        elif name == 'lugar' and self.in_cameras and self.in_content:
            self.lugar = True
        elif name == 'coordenadas' and self.in_cameras and self.in_content:
            self.coord = True

    def characters(self, content):
        if self.video_id:
            self.cam_id += content
        elif self.url:
            self.cam_view += content
        elif self.lugar:
            self.cam_location += content
        elif self.coord:
            coord = content.split(',')
            self.cam_lon += coord[0]
            self.cam_lat += coord[1]

    def endElement(self, name):
        if name == 'camaras':
            self.in_cameras = False
        elif name == 'camara':
            self.in_content = False
            camera = Camera(camera_id=self.cam_id, lat=self.cam_lat, lon=self.cam_lon,
                            camera_view=self.cam_view,
                            camera_location=self.cam_location, comments_number=0)
            if not Camera.objects.filter(camera_id=self.cam_id).exists():
                camera.save()
            self.cam_id = 'LIS1-'
            self.cam_view = ''
            self.cam_lat = ''
            self.cam_lon = ''
            self.cam_location = ''

        elif name == 'id':
            self.video_id = False
        elif name == 'src':
            self.url = False
        elif name == 'lugar':
            self.lugar = False
        elif name == 'coordenadas':
            self.coord = False


class Parserl2(ContentHandler):

    def __init__(self):
        self.url = False
        self.lat = False
        self.lon = False
        self.place = False
        self.in_content = False
        self.in_cameras = False
        self.cam_id = 'LIS2-'
        self.cam_view = ''
        self.cam_lat = ''
        self.cam_lon = ''
        self.cam_location = ''

    def startElement(self, name, attrs):
        if name == 'list':
            self.in_cameras = True
        elif name == 'cam':
            self.in_content = True
            self.cam_id += attrs.getValue('id')
        elif name == 'info' and self.in_cameras and self.in_content:
            self.place = True
        elif name == 'url' and self.in_cameras and self.in_content:
            self.url = True
        elif name == 'latitude' and self.in_cameras and self.in_content:
            self.lat = True
        elif name == 'longitude' and self.in_cameras and self.in_content:
            self.lon = True

    def characters(self, content):
        if self.url:
            self.cam_view += content
        elif self.place:
            self.cam_location += content
        elif self.lat:
            self.cam_lat += content
        elif self.lon:
            self.cam_lon += content

    def endElement(self, name):
        if name == 'list':
            self.in_cameras = False
        elif name == 'cam':
            self.in_content = False
            camera = Camera(camera_id=self.cam_id, lat=self.cam_lat, lon=self.cam_lon,
                            camera_view=self.cam_view,
                            camera_location=self.cam_location,  comments_number=0)
            if not Camera.objects.filter(camera_id=self.cam_id).exists():
                camera.save()
            self.cam_id = 'LIS2-'
            self.cam_view = ''
            self.cam_lat = ''
            self.cam_lon = ''
            self.cam_location = ''

        elif name == 'url':
            self.url = False
        elif name == 'info':
            self.place = False
        elif name == 'latitude':
            self.lat = False
        elif name == 'longitude':
            self.lon = False
