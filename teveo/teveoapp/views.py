import base64
import random
import string
import urllib.request
import requests
from xml.sax import parseString

from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
from django.template import loader
from django.utils import timezone

from .forms import CommentForm, UserForm, FontSize
from .models import User, Camera, Comment, Like
from .parser import Parserl1, Parserl2

# Create your views here.

EXPIRES = 'Mon, 01 Jan 2050 00:00:00 GMT'


def getuser(request):
    if request.COOKIES.get('user'):
        user_id = request.COOKIES.get('user')
        try:
            user = User.objects.get(user_id=user_id)
        except ObjectDoesNotExist:
            user = None
    else:
        user = None
    return user


def createuser():
    user_id = ''.join(random.choices(string.ascii_lowercase + string.digits, k=32))
    while User.objects.filter(user_id=user_id).exists():
        user_id = ''.join(random.choices(string.ascii_lowercase + string.digits, k=32))
    user = User(user_id=user_id)
    user.save()
    return user


def index(request):
    user = getuser(request)
    new = False
    if user is None:
        user = createuser()
        new = True
    comments = Comment.objects.all()
    comments = comments.order_by('-date')
    num_cameras = Camera.objects.count()
    num_comments = Comment.objects.count()
    paginator = Paginator(comments, 10)
    num_pages = paginator.num_pages
    print(num_pages)
    page_number = request.GET.get('page')
    if page_number:
        if int(page_number) <= 0:
            page_number = 1
        page_obj = paginator.get_page(page_number)
    else:
        page_number = 1
        page_obj = paginator.get_page(page_number)
    print(page_number)
    context = {'comments': page_obj, 'num_cameras': num_cameras, 'num_comments': num_comments,
               'user': user, 'page_number': int(page_number), 'num_pages': int(num_pages)}
    response = render(request, 'teveoapp/index.html', context=context)
    if new:
        response.set_cookie('user', user.user_id, expires=EXPIRES)
    return response


@csrf_exempt
def camaras(request):
    if request.method == 'POST':
        lista = request.POST['Lista']
        if lista == '1':
            url = urllib.request.urlopen(
                'https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado1'
                '.xml')
            data = url.read().decode('utf-8')
            parseString(data, Parserl1())
        elif lista == '2':
            url = urllib.request.urlopen(
                'https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado2.xml')
            data = url.read().decode('utf-8')
            parseString(data, Parserl2())
    cameras = Camera.objects.order_by('-comments_number')
    num_cameras = Camera.objects.count()
    num_comments = Comment.objects.count()
    user = getuser(request)
    new = False
    if user is None:
        user = createuser()
        new = True
    if num_cameras >= 1:
        rand_camera = Camera.objects.order_by('?')
        rand_camera = rand_camera.last()
    else:
        rand_camera = None
    response = render(request, 'teveoapp/camaras.html', context={'cam': cameras, 'rand_camera': rand_camera,
                                                                 'num_cameras': num_cameras,
                                                                 'num_comments': num_comments,
                                                                 'user': user})
    if new:
        response.set_cookie('user', user.user_id, expires=EXPIRES)
    return response


def imagen(request):
    camera_id = request.GET.get('cam_id')
    try:
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0'
        }
        camera = Camera.objects.get(camera_id=camera_id)
        image_url = camera.camera_view
        image = requests.get(image_url, headers=headers)
        image_b64 = base64.b64encode(image.content).decode('utf-8')
        data = '<img class="cam" src="data:image/jpeg;base64,##content##">'
        data = data.replace('##content##', image_b64)
        response = HttpResponse(data, content_type="image/jpeg")

    except ObjectDoesNotExist:
        response = HttpResponse('<p>Image not found</p>')
    return response


def comentario_dyn(request):
    camera_id = request.GET.get('cam_id')
    user = getuser(request)
    new = False
    if user is None:
        user = createuser()
        new = True
    try:
        camera = Camera.objects.get(camera_id=camera_id)
        comments = Comment.objects.filter(camera_id_id=camera.id)
        comments = comments.order_by('-date')
        num_cameras = Camera.objects.count()
        num_comments = Comment.objects.count()
        comments_html = loader.render_to_string('teveoapp/comments_template.html',
                                                {'comments': comments, 'num_cameras': num_cameras,
                                                 'num_comments': num_comments, 'user': user})
        data = """
               <ul>
                   ##comments##
               </ul >"""
        response = HttpResponse(data.replace('##comments##', comments_html))
    except ObjectDoesNotExist:
        response = HttpResponse('<p>Error</p>')
    if new:
        response.set_cookie('user', user.user_id, expires=EXPIRES)
    return response


def formulario(request):
    form = CommentForm()
    cam_id = request.GET.get('camera_id')
    context = {
        'form': form, 'camera_id': cam_id
    }

    return render(request, 'teveoapp/formulario.html', context)


def camara(request, cam_id):
    num_cameras = Camera.objects.count()
    num_comments = Comment.objects.count()
    user = getuser(request)
    new = False
    if user is None:
        user = createuser()
        new = True
    try:
        cam = Camera.objects.get(camera_id=cam_id)
        comments = Comment.objects.filter(camera_id_id=cam.id)
        comments = comments.order_by('-date')
        context = {'cam': cam,
                   'comment': comments,
                   'num_cameras': num_cameras,
                   'num_comments': num_comments,
                   'user': user}
        if request.method == 'POST' and 'like' in request.POST:
            try:
                like = Like.objects.get(user_id=user.id, camera=cam.id)
                if like.like is True:
                    like.like = False
                    like.save()
                    likes = Like.objects.filter(like=True, camera=cam.id).count
                    context['likes'] = likes
                    response = render(request, 'teveoapp/like.html', context=context)
                else:
                    like.like = True
                    like.save()
                    likes = Like.objects.filter(like=True, camera=cam.id).count
                    context['likes'] = likes
                    response = render(request, 'teveoapp/dislike.html', context=context)
            except ObjectDoesNotExist:
                like = Like(user_id=user.id, like=True, camera=cam)
                like.save()
                likes = Like.objects.filter(like=True, camera=cam.id).count
                context['likes'] = likes
                response = render(request, 'teveoapp/dislike.html', context=context)
        else:
            try:
                likes = Like.objects.filter(like=True, camera=cam.id).count
                context['likes'] = likes
                like = Like.objects.get(user_id=user.id, camera=cam.id)
                if like.like is True:
                    response = render(request, 'teveoapp/dislike.html', context=context)
                else:
                    response = render(request, 'teveoapp/like.html', context=context)
            except ObjectDoesNotExist:
                response = render(request, 'teveoapp/like.html', context=context)

    except ObjectDoesNotExist:
        response = render(request, 'teveoapp/notfound.html')
    if new:
        response.set_cookie('user', user.user_id, expires=EXPIRES)
    return response


def camara_json(request, cam_id):
    num_cameras = Camera.objects.count()
    num_comments = Comment.objects.count()
    user = getuser(request)
    new = False
    if user is None:
        user = createuser()
        new = True
    try:
        cam = Camera.objects.get(camera_id=cam_id)
        dict_j = {'camera_id': cam.camera_id, 'lat': cam.lat, 'lon': cam.lon, 'camera_view': cam.camera_view,
                  'camera_location': cam.camera_location, 'comments_number': cam.comments_number}
        response = JsonResponse(dict_j)
    except ObjectDoesNotExist:
        response = render(request, 'teveoapp/notfound.html', context={'num_cameras': num_cameras,
                                                                      'num_comments': num_comments, 'user': user})
    if new:
        response.set_cookie('user', user.user_id, expires=EXPIRES)
    return response


def camara_dyn(request, cam_id):
    user = getuser(request)
    new = False
    if user is None:
        user = createuser()
        new = True
    num_cameras = Camera.objects.count()
    num_comments = Comment.objects.count()
    try:
        camera = Camera.objects.get(camera_id=cam_id)
        response = render(request, 'teveoapp/camaradyn.html', context={'cam': camera,
                                                                       'num_cameras': num_cameras,
                                                                       'num_comments': num_comments, 'user': user})
    except ObjectDoesNotExist:
        response = render(request, 'teveoapp/notfound.html', context={'num_cameras': num_cameras,
                                                                      'num_comments': num_comments, 'user': user})
    if new:
        response.set_cookie('user', user.user_id, expires=EXPIRES)
    return response


def save_img(url, filename):
    path = 'teveoapp/static/teveoapp/' + filename + '.jpg'
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0'
    }
    image = requests.get(url, headers=headers)
    with open(path, 'wb') as f:
        f.write(image.content)
        f.close()
    name = 'teveoapp/' + filename + '.jpg'
    return name


def increment_comments(camera_id):
    cam = Camera.objects.get(camera_id=camera_id)
    cam.comments_number += 1
    cam.save()


def comentario(request):
    cam_id = request.GET.get('cam_id')
    try:
        cam = Camera.objects.get(camera_id=cam_id)
        url = cam.camera_view
        user = getuser(request)
        new = False
        date = timezone.now()
        if user is None:
            user = createuser()
            new = True
        if request.method == 'POST':
            form = CommentForm(request.POST)
            if form.is_valid():
                increment_comments(cam_id)
                obj = form.save(commit=False)  # Crea un objeto sin llegar a guardarlo en la bd
                obj.date = date
                obj.camera_id = cam
                obj.user = user
                img_name = cam_id + user.user_id + date.__str__()
                obj.image = save_img(url, img_name)
                obj.save()
                cam = Camera.objects.get(camera_id=cam_id)
            else:
                cam = Camera.objects.get(camera_id=cam_id)
                url = cam.camera_view
        form = CommentForm()
        num_cameras = Camera.objects.count()
        num_comments = Comment.objects.count()
        response = render(request, 'teveoapp/comentario.html', context={'cam': cam, 'image': url, 'fecha': date,
                                                                        'form': form, 'num_cameras': num_cameras,
                                                                        'num_comments': num_comments, 'user': user})
        if new:
            response.set_cookie('user', user.user_id, expires=EXPIRES)
    except ObjectDoesNotExist:
        response = render(request, 'teveoapp/notfound.html', status=404)
    return response


def username(request, user):
    num_cameras = Camera.objects.count()
    num_comments = Comment.objects.count()
    form = UserForm()
    form_font_size = FontSize()
    if 'username' in request.POST and User.objects.filter(username=request.POST['username']).exists():
        user = getuser(request)
        response = render(request, 'teveoapp/in_use.html', context={'form': form,
                                                                    'form_font_size': form_font_size,
                                                                    'num_cameras': num_cameras,
                                                                    'num_comments': num_comments, 'user': user})
    else:
        if 'username' in request.POST:
            user.username = request.POST['username']
            user.save()
        elif 'user_letter_size' in request.POST:
            user.user_letter_size = request.POST['user_letter_size']
            user.save()
        elif 'font' in request.POST:
            user.user_font = request.POST['font']
            user.save()
        response = render(request, 'teveoapp/config.html', context={'form': form,
                                                                    'form_font_size': form_font_size,
                                                                    'num_cameras': num_cameras,
                                                                    'num_comments': num_comments, 'user': user})
    return response


def ayuda(request):
    user = getuser(request)
    new = False
    if user is None:
        user = createuser()
        new = True
    num_cameras = Camera.objects.count()
    num_comments = Comment.objects.count()
    response = render(request, 'teveoapp/ayuda.html', context={'num_cameras': num_cameras,
                                                               'num_comments': num_comments, 'user': user})
    if new:
        response.set_cookie('user', user.user_id, expires=EXPIRES)
    return response


def configuracion(request):
    num_cameras = Camera.objects.count()
    num_comments = Comment.objects.count()
    if request.method == 'GET':
        form = UserForm
        form_font_size = FontSize
        user_id = request.GET.get('id')
        if user_id:
            user = User.objects.get(user_id=user_id)
            response = render(request, 'teveoapp/config.html', context={'form': form,
                                                                        'form_font_size': form_font_size,
                                                                        'num_cameras': num_cameras,
                                                                        'num_comments': num_comments, 'user': user})
            response.set_cookie('user', user.user_id, expires=EXPIRES)
        else:
            user = getuser(request)
            new = False
            if user is None:
                user = createuser()
                new = True
            form = UserForm()
            form_font_size = FontSize()
            response = render(request, 'teveoapp/config.html', context={'form': form,
                                                                        'form_font_size': form_font_size,
                                                                        'num_cameras': num_cameras,
                                                                        'num_comments': num_comments, 'user': user})
            if new:
                response.set_cookie('user', user.user_id, expires=EXPIRES)

    elif request.method == 'POST':
        finish = request.POST.get('finish')
        if finish:
            user = None
        else:
            user = getuser(request)
        new = False
        if user is None:
            user = createuser()
            new = True
        response = username(request, user)
        if new:
            response.set_cookie('user', user.user_id, expires=EXPIRES)
    else:
        user = getuser(request)
        new = False
        if user is None:
            user = createuser()
            new = True
        form = UserForm()
        form_font_size = FontSize()
        response = render(request, 'teveoapp/config.html', context={'form': form,
                                                                    'form_font_size': form_font_size,
                                                                    'num_cameras': num_cameras,
                                                                    'num_comments': num_comments, 'user': user})
        if new:
            response.set_cookie('user', user.user_id, expires=EXPIRES)
    return response


def style(request):
    user = getuser(request)
    if user is None:
        user = createuser()
    size = user.user_letter_size
    css = """  body{
            font-family: """ + user.user_font + """;
            font-size:""" + size.__str__() + """px;
            }
       """
    return HttpResponse(css, content_type='text/css')
