from django.urls import path
from . import views
urlpatterns = [
    path('', views.index, name='index'),
    path('camaras/', views.camaras, name='camaras'),
    path('ayuda/', views.ayuda, name='ayuda'),
    path('imagen/', views.imagen, name='imagen'),
    path('main.css', views.style, name='main.css'),
    path('comentario-dyn/', views.comentario_dyn, name='comentario dinámico'),
    path('configuracion/', views.configuracion, name='configuración'),
    path('comentario/', views.comentario, name='comentario'),
    path('formulario/', views.formulario, name='formulario'),
    path('<str:cam_id>-dyn/', views.camara_dyn, name='cámara dinámica'),
    path('<str:cam_id>.json', views.camara_json, name='cámara json'),
    path('<str:cam_id>/', views.camara, name='cámara'),
]
