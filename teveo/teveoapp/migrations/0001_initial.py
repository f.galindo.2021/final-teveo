# Generated by Django 5.0.3 on 2024-05-17 10:22

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Camera",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("camera_id", models.CharField(max_length=200)),
                ("lat", models.CharField(max_length=100)),
                ("lon", models.CharField(max_length=100)),
                ("camera_view", models.CharField(max_length=200)),
                ("camera_location", models.CharField(max_length=200)),
                ("comments_number", models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name="User",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("username", models.CharField(default="Anónimo", max_length=100)),
                ("user_id", models.CharField(max_length=200)),
                ("user_font", models.CharField(default="sans-serif", max_length=100)),
                ("user_letter_size", models.IntegerField(default=15)),
            ],
        ),
        migrations.CreateModel(
            name="Like",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("like", models.BooleanField(default=False)),
                (
                    "camera",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="teveoapp.camera",
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="teveoapp.user"
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Comment",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("date", models.DateTimeField()),
                ("image", models.ImageField(upload_to="")),
                ("comment", models.TextField(max_length=200)),
                (
                    "camera_id",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="teveoapp.camera",
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="teveoapp.user"
                    ),
                ),
            ],
        ),
    ]
