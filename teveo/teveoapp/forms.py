from django import forms
from .models import Comment, User


class CommentForm(forms.ModelForm):
    class Meta:
        labels = {
            'comment': 'Tu comentario',
        }
        model = Comment
        fields = 'comment',


class UserForm(forms.ModelForm):
    class Meta:
        labels = {
            'username': 'Nombre de usuario',
        }
        model = User
        fields = 'username',


class FontSize(forms.ModelForm):
    class Meta:
        labels = {
            'user_letter_size': 'Tamaño de fuente',
        }
        model = User
        fields = 'user_letter_size',
