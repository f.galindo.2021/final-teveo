# Final-TeVeO


# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Fernando Galindo Díaz
* Titulación: Grado en Ingeniería en Sistemas de Telecomunicación
* Cuenta en laboratorios: fgalindo
* Cuenta URJC: f.galindo.2021
* Video básico (url): https://www.youtube.com/watch?v=unDNv7neMhQ
* Video parte opcional (url): https://www.youtube.com/watch?v=8tiK9cmiv9s
* Despliegue (url): http://fernandogd03.pythonanywhere.com/teveo/
* Contraseñas:
* Cuenta Admin Site: fgalindo/admint7?a2

## Resumen parte obligatoria
Aplicación con un recurso principal para ver el listado de los comentarios puestos en las cámaras por los usuarios.
Permite ver las cámaras de un listado de cámaras disponible desde la página de cámaras, además de poder comentarlas.
También es posible descargar los datos de las cámaras en formato JSON. En la página de la cámara dinámica se puede hacer
lo mismo pero la imagen y los comentarios de la cámara se recargan cada 30 segundos Existe una página de configuración
para elegir el nombre de comentador, tamaño y tipo de fuente de la página y un enlace para exportar la configuración a 
otro navegador.

## Lista partes opcionales

* Nombre parte: Paginación de la página principal
* Nombre parte: Añadido botón de me gusta en la página de cada cámara
* Nombre parte: Añadido botón para cerrar sesión en la página de configuración
* Nombre parte: Añadido favicon