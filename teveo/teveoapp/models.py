from django.db import models


# Create your models here.


class Camera(models.Model):
    camera_id = models.CharField(max_length=200)  # Id de la cámara
    lat = models.CharField(max_length=100)  # Latitud
    lon = models.CharField(max_length=100)  # Longitud
    camera_view = models.CharField(max_length=200)  # Enlace de la cámara
    camera_location = models.CharField(max_length=200)  # Localización de la cámara
    comments_number = models.IntegerField()


class User(models.Model):
    username = models.CharField(max_length=100, default="Anónimo")  # Nombre de usuario
    user_id = models.CharField(max_length=200)
    user_font = models.CharField(max_length=100, default="sans-serif")
    user_letter_size = models.IntegerField(default=15)


class Like(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    camera = models.ForeignKey(Camera, on_delete=models.CASCADE)
    like = models.BooleanField(default=False)


class Comment(models.Model):
    date = models.DateTimeField()  # Fecha del comentario
    user = models.ForeignKey(User, on_delete=models.CASCADE)  # Usuario que ha comentado
    image = models.ImageField()     # Imagen del momento en el que se comenta
    comment = models.TextField(max_length=200)  # Comentario
    camera_id = models.ForeignKey(Camera, on_delete=models.CASCADE)  # Cámara que se está comentando
